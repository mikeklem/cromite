*.idl;chrome/common/extensions
*.idl;extensions/common/api
*.idl;third_party/blink/renderer/modules
*.idl;third_party/blink/renderer/extensions
*.idl;third_party/blink/renderer/core

third_party/blink/renderer/core/frame/settings.json5
third_party/blink/renderer/core/events/event_type_names.json5
third_party/blink/public/mojom/use_counter/metrics/web_feature.mojom

chrome/android/java/AndroidManifest.xml

chrome/browser/ui/tab_helpers.cc
chrome/browser/chrome_browser_interface_binders.cc
chrome/renderer/chrome_content_renderer_client.cc

content/child/runtime_features.cc
content/public/browser/content_browser_client.cc

services/network/network_context.cc
